# Desafio DevOps - Kubernetes Engine #

Pipeline de CI\CD para implantação da aplicação de Comentários em versão API (backend) desenvolvida em Python.

### Quais ferramentas foram utilizadas no Desafio DevOps - Kubernetes Engine ###

* Repositório Git: Bitbucket Cloud
* Orquestrador CI\CD: Bitbucket Pipeline - bitbucket-pipelines.yml
* Infrastructure as code: YAML setup LoadBalancer, replicas, containers, etc - deployment.yaml
* Container Docker da aplicação: Dockerfile
* Gerenciamento de Release: Container Registry - Tag para cada Build de imagem
* Recursos hosts: Kubernetes Engine
* Segurança: Service Account - integração dos ambientes
* Gatilho para gerar Release: Commit na branch Master
* Monitoramento: GKE

### Deploy da aplicação ###

* [Link da aplicação - matéria 1](http://34.70.54.160/api/comment/list/1)
* curl -sv http://34.70.54.160/api/comment/list/1
* [Link da aplicação - matéria 2](http://34.70.54.160/api/comment/list/2)
* curl -sv http://34.70.54.160/api/comment/list/2

### Teste da aplicação - matéria 1 ###

* curl -sv http://34.70.54.160/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"alice@example.com","comment":"first post!","content_id":1}'
* curl -sv http://34.70.54.160/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"alice@example.com","comment":"ok, now I am gonna say something more useful","content_id":1}'
* curl -sv http://34.70.54.160/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"bob@example.com","comment":"I agree","content_id":1}'

### Teste da aplicação - matéria 2 ###

* curl -sv http://34.70.54.160/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"bob@example.com","comment":"I guess this is a good thing","content_id":2}'
* curl -sv http://34.70.54.160/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"charlie@example.com","comment":"Indeed, dear Bob, I believe so as well","content_id":2}'
* curl -sv http://34.70.54.160/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"eve@example.com","comment":"Nah, you both are wrong","content_id":2}'